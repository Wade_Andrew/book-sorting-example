import { useState } from 'react';
import './App.css';
import List from './BookList';

function App() {

  const [books, setBooks] = useState([
    {deleteFlag: false, title:"The Bible", date: new Date('1995-12-17T03:24:00')},
    {deleteFlag: false, title:"Where the Red Fern Grows", date: new Date('1995-12-17T03:24:00')},
    {deleteFlag: false, title:"Robinson Crusoe", date: new Date('1995-12-17T03:24:00')}
  ]);

  const [newTitle, setNewTitle] = useState('');
  const [searchTerm, setSearch] = useState('');
  const [sortType, setSortType] = useState('date'); // 'alpha' or 'date'

  function addBook() {
    if(!newTitle) return;
    const newBooks = books.map(book => {
      return {...book}
    });
    setBooks([...books, {deleteFlag: false, title: newTitle, date: new Date()}]);
    setNewTitle("");
  }

  function deleteToggle(i) {
    const newBooks = books.map(book => {
      return {...book}
    });
    newBooks[i].deleteFlag = !newBooks[i].deleteFlag;
    setBooks(newBooks);
  }

  function deleteBooks() {
    let newBooks = books.map(book => {
      return {...book}
    });
    newBooks = newBooks.filter(book => book.deleteFlag === false);
    setBooks(newBooks);
  }

  function updateSort(e) {
    setSortType(e.target.value);
  }

  const deleteDisabled = books.reduce((acc, book) => book.deleteFlag === false && acc, true);

  let sortedBooks = [...books];
  // sort affects original array
  if(sortType==='alpha') {
    sortedBooks.sort((book1,book2) => book1.title > book2.title ? 1: -1);
  } else {
    sortedBooks.sort((book1,book2) => book1.date - book2.date);
  }
  // filter does not affect original array, but returns a new array
  if(searchTerm) {
    sortedBooks = sortedBooks.filter(book => book.title.includes(searchTerm));
  }

  return (
    <div className="App">
      <div>
        <button onClick={() => addBook()}>Add</button>
        <button disabled={deleteDisabled} onClick={deleteBooks}>Delete</button>
      </div>
      <div>
        <input type="text" value={newTitle} placeholder="New Book Title" onChange={e=>setNewTitle(e.target.value)}></input>
      </div>
      <div>
        🔍 <input type="text" value={searchTerm} placeholder="Search..." onChange={e=>setSearch(e.target.value)}></input>
      </div>
      <div>
        Sort by: 
        <input type="radio" value="date" onChange={updateSort} name="date" checked={sortType==='date'}></input><label for="date">Date</label>
        <input type="radio" value="alpha" onChange={updateSort} name="alpha" checked={sortType==='alpha'}></input><label for="date">Alphabetical</label>
      </div>
      <List books={sortedBooks} delete={deleteToggle}></List>
    </div>
  );
}

export default App;
